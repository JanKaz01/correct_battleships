package com.app.service;

import com.app.model.Board;
import com.app.model.User;

import java.util.Scanner;

import static java.lang.System.out;

public class UserService {
    public static User generateUser(int width) {
        var sc = new Scanner(System.in);
        var board = new Board(width);
        out.println("Enter your nickname: ");
        var nickname = sc.nextLine();

        return User.builder().nickname(nickname).board(board).build();
    }
}
