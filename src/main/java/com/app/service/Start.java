package com.app.service;

import com.app.model.Board;
import com.app.model.User;

import java.util.Scanner;

import static com.app.model.Board.*;
import static com.app.service.ShipService.*;
import static com.app.service.UserService.*;
import static java.lang.System.*;

public class Start {
    public static void start() {
        int width = setBoardSize();
        var user1 = generateUser(width);
        createShips(user1);
        var user2 = generateUser(width);
        createShips(user2);
        var winner = Game.start(user1, user2).getNickname();
        out.println("Congrats! " + winner + " you're a winner!");
    }

    static int setBoardSize() {
        Scanner sc = new Scanner(in);
        out.println("Enter the size of board: ");
        return sc.nextInt();
    }

    static void createShips(User user) {
        setShipsUserCanBuild(user);
    }
}
