package com.app.service;

import com.app.model.User;

import java.util.Scanner;

import static java.lang.System.*;

public class Game {
    public static User start(User user1, User user2){
        User winner = null;

        while(winner == null){
            move(user1, user2);
            move(user2, user1);
            winner = isGameEnded(user1, user2);
        }

        return winner;
    }

    private static void move(User user1, User user2){
        Scanner sc = new Scanner(in);

        boolean isShotGiven = false;
        String result;

        out.println(user1.getNickname() + " it's your turn!");
        out.println();
        user1.getBoard().displayBoard();
        out.println();

        while(!isShotGiven) {
            out.println("Please enter coordinates for the shot: ");
            int x = sc.nextInt();
            int y = sc.nextInt();

            result = user1.move(x, y, user2);
            out.println(result);

            if(!result.equals("You should not shoot here!")){
                isShotGiven = true;
            }
        }
    }

    private static User isGameEnded(User user1, User user2){
        if(isUserLost(user1)){
            return user2;
        }
        else if(isUserLost(user2)){
            return user1;
        }
        return null;
    }

    private static boolean isUserLost(User user){
        int[][] arr = user.getBoard().getBoard();

        for(int i = 1; i < arr.length; i++){
            for(int j = 1; j < arr[0].length; j++){
                if(arr[i][j] == 1){
                    return false;
                }
            }
        }

        return true;
    }
}
