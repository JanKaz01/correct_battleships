package com.app.service;

import com.app.model.Board;
import com.app.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.out;

public class ShipService {

    public static boolean generateShip(Board board, int x, int y, int width, char direction) {
        switch (direction) {
            case 'p':
                if (!checkField(board, x, y, width, direction)) return false;

                for (int i = 0; i < width; i++) {
                    board.getBoard()[x][y + i] = 1;
                }
                break;
            case 'd':
                if (!checkField(board, x, y, width, direction)) return false;

                for (int i = 0; i < width; i++) {
                    board.getBoard()[x - i][y] = 1;
                }
                break;
            default:
                break;
        }

        return true;
    }

    private static boolean checkField(Board board, int x, int y, int width, char direction) {
        checkBorder(x, y, board);

        switch (direction) {
            case 'p': {
                for (int i = y; i < y + width; i++) {
                    //SPRAWDZANIE WARUNKOW BRZEGOWYCH
                    if (x == 1 & y == 1) { //jezeli statek zaczyna sie w lewym gornym narozniku
                        if (board.getBoard()[x + 1][i] != 0
                                || board.getBoard()[x + 1][i + 1] != 0
                                || i + 1 > board.getWidth() // jezeli indeks za ostatnim elementem miesci sie w boardzie
                                || board.getBoard()[x][i + 1] != 0) {
                            return false;
                        }
                    } else if (x == 1 && y == board.getWidth()) { //jezeli statek zaczyna sie w prawym gornym narozniku
                        if (board.getBoard()[x][i - 1] != 0 || board.getBoard()[x + 1][i - 1] != 0
                                || board.getBoard()[x + 1][i] != 0) {
                            return false;
                        }
                    } else if (x == board.getWidth() && y == 1) { //jezeli statek zaczyna sie w lewym dolnym narozniku
                        if (i + 1 < board.getWidth() && (board.getBoard()[x - 1][i] != 0
                                || board.getBoard()[x - 1][i + 1] != 0 || board.getBoard()[x][i + 1] != 0)) {
                            return false;
                        }
                    } else if (x == board.getWidth() && y == board.getWidth()) { //jezeli statek zaczyna sie w prawym dolnym narozniku
                        if (board.getBoard()[x][i - 1] != 0 || board.getBoard()[x - 1][i - 1] != 0
                                || board.getBoard()[x - 1][i] != 0) {
                            return false;
                        }
                    } else if (y == 1) { // jezeli statek zaczyna sie przy lewej krawedzi ale nie w narozniku
                        if (board.getBoard()[x - 1][i] != 0 || i + 1 != board.getWidth()
                                || board.getBoard()[x - 1][i + 1] != 0
                                || board.getBoard()[x][i + 1] != 0
                                || board.getBoard()[x + 1][i] != 0
                                || board.getBoard()[x + 1][i + 1] != 0) {
                            return false;
                        }
                    } else if (x == 1) { //jezeli statek zaczyna sie w pierwszym rzedzie ale nie w naroznik
                        if (board.getBoard()[x][i - 1] != 0
                                || board.getBoard()[x + 1][i - 1] != 0
                                || board.getBoard()[x + 1][i] != 0
                                || i + 1 < board.getWidth()
                                || board.getBoard()[x + 1][i + 1] != 0
                                || board.getBoard()[x + 1][i + 1] != 0) {
                            return false;
                        }
                    } else if (x == board.getWidth()) { // jezeli statek zaczyna sie w ostatnim rzedzie ale nie w naroznikku
                        if (board.getBoard()[x][i - 1] != 0
                                || board.getBoard()[x - 1][i - 1] != 0
                                || board.getBoard()[x - 1][i] != 0
                                || i + 1 < board.getWidth()
                                || board.getBoard()[x - 1][i + 1] != 0
                                || board.getBoard()[x][i + 1] != 0) {
                            return false;
                        }
                    } else {// WARUNKI JEŻELI STATEK NIE JEST NA POZYCJI BRZEGOWEJ
                        //warunek na sprawdzanie trzech brzegowych elementow przed pierwszym indeksie statku
                        if (i == y && (board.getBoard()[x][i - 1] != 0 || board.getBoard()[x + 1][i - 1] != 0 ||
                                board.getBoard()[x - 1][i - 1] != 0)) {
                            return false;
                        }
                        //warunek na sprawdzanie gornych i dolnych pol nad/pod statkiem
                        if (board.getBoard()[x + 1][i] != 0 || board.getBoard()[x - 1][i] != 0) {
                            return false;
                        }
                        //warunek na sprawdzanie trzech brzegowych elementow za ostatnim indeksem statku
                        if (i == y + width && (board.getBoard()[x][i + 1] != 0 || board.getBoard()[x + 1][i + 1] != 0 ||
                                board.getBoard()[x - 1][i + 1] != 0)) {
                            return false;
                        }
                    }
                }
                break;
            }
            case 'd':
                for (int i = x; i < x + width; i++) {
                    //SPRAWDZANIE WARUNKÓW BRZEGOWYCH
                    if (x == 1 & y == 1) { //jezeli statek zaczyna sie w lewym gornym narozniku
                        if (y + 1 < board.getWidth() && (board.getBoard()[i][y + 1] != 0
                                || board.getBoard()[i + 1][y + 1] != 0
                                || board.getBoard()[i + 1][y] != 0)) { // jezeli indeks za ostatnim elementem miesci sie w boardzie {
                            return false;
                        }
                    } else if (x == 1 && y == board.getWidth()) { //jezeli statek zaczyna sie w prawym gornym narozniku
                        if (i + 1 < board.getWidth() && (board.getBoard()[i][y - 1] != 0
                                || board.getBoard()[i + 1][y] != 0
                                || board.getBoard()[i + 1][y - 1] != 0)) {
                            return false;
                        }
                    } else if (x == board.getWidth() && y == 1) { //jezeli statek zaczyna sie w lewym dolnym narozniku
                        if (board.getBoard()[i - 1][y] != 0
                                || board.getBoard()[i - 1][y + 1] != 0
                                || board.getBoard()[i][y + 1] != 0) {
                            return false;
                        }
                    } else if (x == board.getWidth() && y == board.getWidth()) { //jezeli statek zaczyna sie w prawym dolnym narozniku
                        if (board.getBoard()[i][y - 1] != 0
                                || board.getBoard()[i - 1][y - 1] != 0
                                || board.getBoard()[i - 1][y] != 0) {
                            return false;
                        }
                    } else if (y == 1) { // jezeli statek zaczyna sie przy lewej krawedzi ale nie w narozniku
                        if (board.getBoard()[i - 1][y] != 0
                                || board.getBoard()[i - 1][y + 1] != 0
                                || board.getBoard()[i][y + 1] != 0
                                || i + 1 < board.getWidth()
                                || board.getBoard()[i + 1][y] != 0
                                || board.getBoard()[i + 1][y + 1] != 0) {
                            return false;
                        }
                    } else if (x == 1) { //jezeli statek zaczyna sie w pierwszym rzedzie ale nie w narozniku
                        if (board.getBoard()[i][y - 1] != 0
                                || board.getBoard()[i][y + 1] != 0
                                || i + 1 < board.getWidth()
                                || board.getBoard()[i + 1][y] != 0
                                || board.getBoard()[i + 1][y + 1] != 0
                                || board.getBoard()[i + 1][y - 1] != 0) {
                            return false;
                        }
                    } else if (x == board.getWidth()) { // jezeli statek zaczyna sie w ostatnim rzedzie ale nie w naroznikku
                        if (width == 1
                                || board.getBoard()[i][y - 1] != 0
                                || board.getBoard()[i - 1][y - 1] != 0
                                || board.getBoard()[i - 1][y] != 0
                                || board.getBoard()[i - 1][y + 1] != 0
                                || board.getBoard()[i][y + 1] != 0) {
                            return false;
                        }
                    } else {
                        if (i == x && (board.getBoard()[i + 1][y] != 0 || board.getBoard()[i + 1][y + 1] != 0 ||
                                board.getBoard()[i + 1][y - 1] != 0)) {
                            return false;
                        }
                        if (board.getBoard()[i][y + 1] != 0 || board.getBoard()[i][y - 1] != 0) {
                            return false;
                        }
                        if (i == x + width && (board.getBoard()[i - 1][y + 1] != 0 || board.getBoard()[i - 1][y + 1] != 0 ||
                                board.getBoard()[x - 1][y + 1] != 0)) {
                            return false;
                        }
                    }
                }
                break;
            default:
                break;
        }

        return true;
    }

    private static void checkBorder(int x, int y, Board board) {
        if (x > board.getWidth()) {
            throw new IllegalArgumentException();
        }
        if (y > board.getWidth()) {
            throw new IllegalArgumentException();
        }
        if (y < 1) {
            throw new IllegalArgumentException();
        }
        if (x < 1) {
            throw new IllegalArgumentException();
        }
    }

    private static int lackOfShips(List<Integer> ships, int width) {
        var sum = ships.stream().reduce(0, Integer::sum);
        return width - sum;
    }

    private static List<Integer> shipsUserCanBuild(int width) {
        List<Integer> list = new ArrayList<>();
        list.add((int) Math.floor(width * 0.4));
        list.add((int) Math.floor(width * 0.3));
        list.add((int) Math.floor(width * 0.2));
        list.add((int) Math.floor(width * 0.1));

        return list;
    }

    public static void setShipsUserCanBuild(User user) {
        var ships = shipsUserCanBuild(user.getBoard().getWidth() - 1);

        var singleMasted = ships.get(0);
        var doubleMasted = ships.get(1);
        var thirthMasted = ships.get(2);
        var fourthMasted = ships.get(3);

        while (ships.get(0) > 0) {
            ships.set(0, singleMasted--);
            xxx(1, user.getBoard());
        }
        while (ships.get(1) > 0) {
            ships.set(1, doubleMasted--);
            xxx(2, user.getBoard());
        }
        while (ships.get(2) > 0) {
            ships.set(2, thirthMasted--);
            xxx(3, user.getBoard());
        }
        while (ships.get(3) > 0) {
            ships.set(3, fourthMasted--);
            xxx(4, user.getBoard());
        }
    }

    private static void xxx(int width, Board board) {
        int x, y;
        char direction;
        Scanner sc = new Scanner(System.in);
        do {
            out.println("Now you are generating ships with: " + width + " width");
            out.println("Enter the x value of first index of ship you wanna place: ");
            x = sc.nextInt();
            out.println("Enter the y value of first index of ship you wanna place: ");
            y = sc.nextInt();
            if (width > 1) {
                out.println("Enter the direction (d/p) of ship you wanna place: ");
                do {
                    direction = sc.next().charAt(0);
                    if(direction != 'p' && direction != 'd') {
                        out.println("Wrong direction!");
                        out.println("Enter the direction (d/p) of ship you wanna place: ");
                    }
                } while (direction != 'p' && direction != 'd');

            } else {
                direction = 'p';
            }

            boolean ended = generateShip(board, x, y, width, direction);
            if (!ended) {
                out.println("You can't put ship here! Repeat.");
            }
        } while (!generateShip(board, x, y, width, direction));

        board.displayBoard();
        out.println("Good job! You have just generated your ship!");
    }
}
