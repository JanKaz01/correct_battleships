package com.app.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Data
@EqualsAndHashCode
public class User {
    String nickname;
    Board board;

    public String move(int x, int y, User user) {
        var board = user.getBoard();
        var possibility = board.isItPossibleToGetShot(x, y);
        if (possibility == 0) {
            board.getBoard()[x][y] = '3';
            return "Misshot!";
        } else if (possibility == 1) {
            board.getBoard()[x][y] = '2';
            return "Good shoot!";
        } else {
            return "You should not shoot here!";
        }
    }
}
