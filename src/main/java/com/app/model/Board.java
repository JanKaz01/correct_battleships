package com.app.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode
@ToString
public class Board {
    int width;
    int[][] board;

    public Board(int width) {
        this.width = width;
        this.board = generateBoard(width);
    }

    private int[][] generateBoard(int width) {
        int[][] board = new int[width + 1][width + 1];
        for (int i = 1; i < board[0].length; i++) {
            board[0][i] = i;
        }

        for (int i = 1; i < board.length; i++) {
            board[i][0] = i;
        }

        for (int i = 1; i < board.length; i++) {
            for (int j = 1; j < board[i].length; j++) {
                board[i][j] = 0;
            }
        }

        return board;
    }

    public int isItPossibleToGetShot(int x, int y) {
        return board[x][y] == 0 ? 0 : board[x][y] == 1 ? 1 : 3;
    }

    public void displayBoard() { //nad tym trzeba popracować
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + " |");
            }
            System.out.println();
        }
    }
}

